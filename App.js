import React, {useState, useEffect} from 'react';
import {
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
    StyleSheet,
    ActivityIndicator,
    Dimensions,
    Platform,
} from 'react-native';

console.disableYellowBox = true;

const getRandomColor = () => {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

const screenHeight = Dimensions.get('window').height + StatusBar.currentHeight;
const PROPS_STICKED_ELEMENT = 20;

function App() {
    const [tickerPrice, setTickerPrice] = useState(null);
    const [list, setList] = useState([]);
    const [stickBottom, setStickBottom] = useState(true);

    useEffect(() => {

        let list = [];
        for (let i = 0; i < 50; i++) {
            list.push({
                width: '90%',
                height: 45,
                marginLeft: '5%',
                marginRight: '5%',
                marginTop: 5,
                flex: 1,
                backgroundColor: getRandomColor(),
            });
        }
        setList(list);

        fetch('https://api.cryptonator.com/api/ticker/btc-eth', {method: 'get'})
            .then(response => response.json())
            .then(data => {
                console.log(data);
                setTickerPrice(data.ticker.price);
            });
    }, []);

    function onScroll(event) {

        let shouldBeSticked = 50 * (PROPS_STICKED_ELEMENT + 1) > screenHeight + event.nativeEvent.contentOffset.y;

        if (!stickBottom && shouldBeSticked) {
            setStickBottom(true);
        }
        if (stickBottom && !shouldBeSticked) {
            setStickBottom(false);
        }

    }

    return (
        <View>
            <ScrollView
                stickyHeaderIndices={[PROPS_STICKED_ELEMENT - 1]}
                onScroll={onScroll}
            >
                {list && list.map((rectangle, index) => {
                    return (
                        <View style={rectangle} key={index.toString()}>
                            {!tickerPrice && <ActivityIndicator/>}
                            <Text style={styles.text}>{tickerPrice}</Text>
                            <Text style={styles.text}>{Platform.OS}</Text>
                        </View>
                    );
                })}
            </ScrollView>

            {stickBottom &&
            <View style={[list[PROPS_STICKED_ELEMENT - 1], styles.stickBottom]}>
                {!tickerPrice && <ActivityIndicator/>}
                <Text style={styles.text}>{tickerPrice}</Text>
                <Text style={styles.text}>{Platform.OS}</Text>
            </View>
            }

        </View>
    );
}

const styles = StyleSheet.create({
    text: {
        textAlign: 'center',
    },
    stickBottom: {
        position: 'absolute',
        bottom: 0,
    },
});

export default App;
